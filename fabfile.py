# -*- coding: utf-8 -*-
"""
############################################################
Tutorial de Jogos em Python - Fabric deployment
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2014/10/10  $
:Status: This is a "work in progress"
:Revision: $Revision: 0.01 $
:Home: `Labase <http://labase.nce.ufrj.br/>`__
:Copyright: ©2013, `GPL <http://is.gd/3Udt>__.
"""
from fabric.api import local  # , settings, cd, run, lcd
ORIGIN = "/home/carlo/Documentos/dev/brygame-tut/doc/build/html"
TARGET = "/home/carlo/Documentos/dev/brython-in-the-classroom/pyschool/static/"


def html():
    local("cd /home/carlo/Documentos/dev/brygame-tut/doc")
    local("cp --remove-destination -r %s %s" % (ORIGIN, TARGET))
    #kzip()
