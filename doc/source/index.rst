.. Brython Game Tutorial documentation master file, created by
   sphinx-quickstart on Sat Oct 11 09:50:52 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem Vindos ao Circo Voador da Programação Python
================================================

Aqui vamos ter uma introdução rápida de como programar jogos para Web usando Python.
Na verdade vamos usar o Brython que é o Python que funciona dentro de um navegador web como o Firefox.

.. image:: _static/PPFC.jpg

Sumário
=======

.. toctree::
   :maxdepth: 2

   inicia.rst
   paisagem.rst
   cenario.rst
   animado.rst
   organizado.rst
   tanque.rst
   movimento.rst
   direcao.rst
   colisao.rst
   tiro.rst
   explode.rst

Indices e Tabelas
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

