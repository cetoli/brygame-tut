.. _modulo_animado:

Criando uma Animação
====================

Vamos colocar uma peça animada. A base piramide tem imagens que anima uma luz piscando e um raio que tremula.
O componente SpriteAnimation é que permite que ae crie um rolo de animação (reel) e depois se rode a animação (animate).

.. code-block:: python

    def main():
        from crafty import Crafty
        from browser import document
        # Cria uma janela de 512x512 para o jogo na divisão do documento pydiv
        jogo = Crafty(512, 512, document["pydiv"])
        # Cria um pano de fundo para o jogo usando a imagem stage-bg
        jogo.e("2D, Canvas, Image")\
            .attr(x=0, y=0, w=512, h=512).image("images/stage-bg.png")
        """ -XXX- Aqui teremos que adicionar a base, o objeto escuro em forma de piramide"""
        jogo.sprites(64, 'images/sprites.png',
                   planta=[1, 0], pedra=[1, 1], tanque=[0, 3], torre=[7, 3], tiro=[7, 1])
        jogo.e("Planta, 2D, Canvas, planta").attr(x=0, y=0, w=64, h=64)
        jogo.e("Pedra, 2D, Canvas, pedra").attr(x=128, y=64, w=64, h=64)
        """ -XXX- Aqui vai a peça que é a base animada. O valor -1 significa anima para sempre"""
        jogo.e("Base, 2D, Canvas, SpriteAnimation, base").attr(x=128, y=256, w=64, h=64)\
            .attr(x=128, y=256, w=64, h=64).reel("pisca_base", 1000, 2, 0, 4)\
            .animate("pisca_base", -1)


    if __name__ == "__main__":
        main()

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. note::
   Observe os comentários com *-XXX-* e modifique ou acrescente o que está sendo pedido

