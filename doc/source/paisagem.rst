.. _modulo_paisagem:

Colocando Objetos no Cenário
============================

Vamos colocar outras peças que vão compor o cenário. Vamos Colocar uma pedra e uma planta.


.. code-block:: python

    def main():
        from crafty import Crafty
        from browser import document
        # Cria uma janela de 512x512 para o jogo na divisão do documento pydiv
        jogo = Crafty(512, 512, document["pydiv"])
        #Cria um pano de fundo para o jogo usando a imagem stage-bg
        jogo.e("2D, Canvas, Image")\
            .attr(x=0, y=0, w=512, h=512).image("images/stage-bg.png")
        # Carrega as imagens dos objetos que estão agrupados na figura sprites
        jogo.sprites(64, 'images/sprites.png',
                   planta=[1, 0], pedra=[1, 1], tanque=[0, 3], torre=[7, 3], tiro=[7, 1])
        jogo.e("Planta, 2D, Canvas, planta").attr(x=0, y=0, w=64, h=64)
        jogo.e("Pedra, 2D, Canvas, pedra").attr(x=128, y=64, w=64, h=64)

    if __name__ == "__main__":
        main()

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. note::
   Tudo ainda está acumulado no procedimento main. Precisaremos melhorar esta organização.

