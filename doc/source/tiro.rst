.. _modulo_tiro:

Usando o Teclado Para Atirar
============================

Usando a tecla espaço vamos criar a ação de atirar do tanque.
Vamos criar um objeto tiro que se movimenta no sentido em que o tanque está apontando.
Quando ele esbarra com um objeto ou com a borda ele desaparece, voltando a municiar o tanque.

.. code-block:: python

    class Tiro:
        """Representa o disparo de tiro com a torreta do canhão
        """
        def __init__(self, jogo):
            # O dicionário rumo define uma direção cartesiana para cada sentido em graus
            self.rumo = {90: (256, None), 0: (None, -256),  270: (-256, None), 180: (None, 256)}
            self.tanque = None
            self.tiro = jogo.e("Tiro, 2D, Canvas, Tween, Keyboard, tiro")\
                .attr(" -XXX- complete aqui")
            # Este evento ocorre a cada início do frame de animação
            self.tiro.bind("EnterFrame", self.colide)
            self.tiro.bind("KeyDown", self.atira)
            # Este evento ocorre quando a transformação tween chega ao fim
            self.tiro.bind("TweenEnd", self.para)

        def anexa(self, tanque):
            """Anexa o tiro ao tanque
            """
            self.tanque = tanque
            self.tiro.attr(**tanque.posicao)
            tanque.tanque.attach(self.tiro)

        def atira(self, evento):
            """Captura a tecla ESPAÇO e dispara um tiro
            """
            if evento.key == 32:
                x, y = self.rumo[self.tanque.direcao]
                if not x is None:
                    self.tiro.tween(1000, x=256+x)
                if not y is None:
                    self.tiro.tween(1000, y=256+y)

        def colide(self, evento):
            """Evita a movimentação do tiro se ele colide com objetos
            """
            if self.tiro.hit("Pedra") or self.tiro.hit("Planta")or self.tiro.hit("Base"):
                self.tiro.tween(1, **self.tanque.posicao)

        def para(self, evento):
            """Evita a movimentação do tiro se ele sai do campo
            """
            self.tiro.attr(**self.tanque.posicao)

    class Tanque:
        """Representa o tanque com uma torreta de canhão
        """
        def __init__(self, jogo):
            # O dicionário rumo define uma direção em graus para cada sentido cartesiano
            self.rumo = {(0, 0): None, (1, 0): 90, (0, -1): 0, (-1, 0): 270, (0, 1): 180}
            self.tiro = Tiro(jogo)
            self.tanque = jogo.e("Tanque, 2D, Canvas, tanque")\
                .attr(" -XXX- complete aqui").fourway(1)  # o fourway controla com setas
            self.torre = jogo.e(" -XXX- complete aqui").attr(" -XXX- complete aqui")
            # O comndo attach liga a torre ao tanque para andem juntos
            self.tanque.attach(self.torre)
            self.tiro.anexa(self)
            # O bind associa um procedimento que é executado quando acontece uma mudança de direção
            self.tanque.bind("NewDirection", self.aponta)
            self.tanque.bind("Moved", self.colide)

        @property
        def posicao(self):
            """Retorna a posição do tanque.
            """
            return dict(x=self.tanque.x, y=self.tanque.y)

        @property
        def direcao(self):
            """Retorna a direção do tanque.
            """
            return self.tanque.rotation

        def aponta(self, evento):
            """Corrige a direção do tanque quando se move em outro sentido
            """
            direcao = self.rumo[(evento.x, evento.y)]
            if not direcao is None:
                self.tanque.rotation = direcao

        def colide(self, evento):
            """Evita a movimentação do tanque se ele colide com objetos
            """
            if self.tanque.hit("Pedra") or self.tanque.hit("Planta")or self.tanque.hit("Base"):
                self.tanque.attr(x=evento.x, y=evento.y)

    class Jogo:
        """Representa o jogo e posiciona as principais peças do cenário
        """
        def __init__(self, jogo):
            jogo.e("2D, Canvas, Image")\
                .attr(x=0, y=0, w=512, h=512).image("images/stage-bg.png")
            """ -XXX- Aqui teremos que adicionar a base, o objeto escuro em forma de piramide"""
            jogo.sprites(64, 'images/sprites.png',
                       planta=[1, 0], pedra=[1, 1], tanque=[0, 3], torre=[7, 3], tiro=[7, 1])
            jogo.e("Planta, 2D, Canvas, planta").attr(x=0, y=0, w=64, h=64)
            jogo.e("Pedra, 2D, Canvas, pedra").attr(x=128, y=64, w=64, h=64)
            """ Aqui vai a peça que é a base animada. O valor -1 significa anima para sempre"""
            jogo.e("Base, 2D, Canvas, SpriteAnimation, base").attr(x=128, y=256, w=64, h=64)\
                .attr(x=128, y=256, w=64, h=64).reel("pisca_base", 1000, 2, 0, 4)\
                .animate("pisca_base", -1)


    def main():
        from crafty import Crafty
        from browser import document
        # Cria uma janela de 512x512 para o jogo na divisão do documento pydiv
        jogo = Crafty(512, 512, document["pydiv"])
        # Cria o objeto que representa o Jogo
        Jogo(jogo)
        # Cria o objeto que representa o tanque


    if __name__ == "__main__":
        main()

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. note::
   Observe os comentários com *-XXX-* e modifique ou acrescente o que está sendo pedido

