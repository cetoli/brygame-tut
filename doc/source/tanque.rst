.. _modulo_tanque:


Adicionando o Objeto Tanque
===========================

Agora observe o jeitão da classe jogo e crie uma classe que representa o tanque.
Seja cuidadoso com as tabulações que são essenciais num programa Python.
Basta que algo esteja desalhinhado ou alinhado na tabulação errada para o programa falhar.

.. code-block:: python

    class Tanque:
        """Representa o tanque com uma torreta de canhão
        """
        def __init__(self, jogo):
            pass  # este pass está aqui para dizer que o __init__ está vazio
            # apague estas tres linhas e faça que esta classe crie um tanque
            # contendo uma torreta com canhão

    class Jogo:
        """Representa o jogo e posiciona as principais peças do cenário
        """
        def __init__(self, jogo):
            jogo.e("2D, Canvas, Image")\
                .attr(x=0, y=0, w=512, h=512).image("images/stage-bg.png")
            """ -XXX- Aqui teremos que adicionar a base, o objeto escuro em forma de piramide"""
            jogo.sprites(64, 'images/sprites.png',
                       planta=[1, 0], pedra=[1, 1], tanque=[0, 3], torre=[7, 3], tiro=[7, 1])
            jogo.e("Planta, 2D, Canvas, planta").attr(x=0, y=0, w=64, h=64)
            jogo.e("Pedra, 2D, Canvas, pedra").attr(x=128, y=64, w=64, h=64)
            """ Aqui vai a peça que é a base animada. O valor -1 significa anima para sempre"""
            jogo.e("Base, 2D, Canvas, SpriteAnimation, base").attr(x=128, y=256, w=64, h=64)\
                .attr(x=128, y=256, w=64, h=64).reel("pisca_base", 1000, 2, 0, 4)\
                .animate("pisca_base", -1)


    def main():
        from crafty import Crafty
        from browser import document
        # Cria uma janela de 512x512 para o jogo na divisão do documento pydiv
        jogo = Crafty(512, 512, document["pydiv"])
        # Cria o objeto que representa o Jogo
        Jogo(jogo)
        # Cria o objeto que representa o tanque


    if __name__ == "__main__":
        main()

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. note::
   Observe os comentários com *-XXX-* e modifique ou acrescente o que está sendo pedido

