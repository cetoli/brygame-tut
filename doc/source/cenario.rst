.. _modulo_cenario:

Colocando mais objetos no Cenário
=================================

Vamos colocar mais peças para incrementar o cenário. Observe a figura "sprites" abaixo.
Acrescente novos itens observando a posição na figura. O posicionamente começa em [0,0]

.. image:: _static/sprites.png
    :width: 300px

.. code-block:: python
    :emphasize-lines: 9,10,11,14
    
    def main():
        from crafty import Crafty
        from browser import document
        # Cria uma janela de 512x512 para o jogo na divisão do documento pydiv
        jogo = Crafty(512, 512, document["pydiv"])
        # Cria um pano de fundo para o jogo usando a imagem stage-bg
        jogo.e("2D, Canvas, Image")\
            .attr(x=0, y=0, w=512, h=512).image("images/stage-bg.png")
        """ -XXX- Acrescente novos itens, nomeando eles e definindo a posição na figura tipo: umnome=[x, y]"""
        jogo.sprites(64, 'images/sprites.png',
                   planta=[1, 0], pedra=[1, 1], tanque=[0, 3], torre=[7, 3], tiro=[7, 1])
        jogo.e("Planta, 2D, Canvas, planta").attr(x=0, y=0, w=64, h=64)
        jogo.e("Pedra, 2D, Canvas, pedra").attr(x=128, y=64, w=64, h=64)
        """ -XXX- Adicione aqui novos comandos jogo.e(..).attr(..) para colocar mais peças no cenário"""

    if __name__ == "__main__":
        main()

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. note::
   Observe os comentários com *-XXX-* e modifique ou acrescente o que está sendo pedido

