.. _modulo_explode:

Explodindo e Destruindo as Coisas
=================================

Quando o tiro atinge a base ou uma planta ela explode e desaparece.
Vamos criar uma animação usando a imagem **explosion-sprites.png**.
Esta imagem tem 32 sprites de tamanho 90 pixels.
Presisamos ter acesso a esses objetos no jogo e vamos criar a propriedade Jogo.JOGO
Quando a animação acaba destruimos a própria animação e o objeto atingido.

.. image:: _static/explosion-sprites.png
    :width: 400px

.. code-block:: python

    class Tiro:
        """Representa o disparo de tiro com a torreta do canhão
        """
        def __init__(self, jogo):
            self.jogo = jogo  # vamos usar opara criar a explosão
            self.boom = None  # esta será a explosão
            # ... bla ..bla

        def colide(self, evento):
            """Evita a movimentação do tiro se ele colide com objetos
            """
            planta, base = self.tiro.hit("Planta"), self.tiro.hit("Base")

            def destroy(event):
                if base:
                    Jogo.JOGO.base.destroy()
                else:
                    Jogo.JOGO.planta.destroy()
                self.boom.destroy()
            if self.tiro.hit("Pedra") or base or planta:
                self.tiro.tween(1, **self.tanque.posicao)
            hit = base or planta
            if hit:
                x, y = hit[0].obj.x, hit[0].obj.y  # a função hit retorna o objeto acertado
                self.boom = self.jogo.e("Xplode, 2D, Canvas, SpriteAnimation, explode")\
                    .attr(x=x, y=y, w=64, h=64).reel('XP', 800, 0, 0, 32)\
                    .animate("XP", 1)
                self.boom.bind("AnimationEnd", destroy)

    class Jogo:
        """Representa o jogo e posiciona as principais peças do cenário
        """
        JOGO = None

        def __init__(self, jogo):
            Jogo.JOGO = self
            # ... bla ..bla

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. note::
   Por enquanto é só. Agora fica por conta da sua imaginação

