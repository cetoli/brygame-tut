"""
############################################################
Kwarwp -  Learning to program with a game in Python
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: 2014/10/11
:Status: This is a "work in progress"
:Revision: 0.1.0
:Home: `Labase <http://labase.selfip.org/>`__
:Copyright: 2014, `GPL <http://is.gd/3Udt>`__.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. _mod_crafty

"""
__author__ = 'carlo'
__version__ = "0.1.0"
from crafty import Crafty
from browser import document


class Kwarwp:
    def __init__(self, game):
        self.game = game
        game.sprites(
            42, 'images/kwarwp.png',
            ke=[0, 0], kn=[0, 1], ks=[0, 2], kw=[2, 3],
            kr=[0, 4], kg=[1, 4], kb=[2, 4], pj=[0, 5])

    def person(self):
        self.game.e("Kaio, 2D, Canvas, ke").attr(x=0, y=0, w=64, h=64)
        return self


def main(w=512, h=512, canvas="pydiv"):
    from crafty import Crafty
    from browser import document
    game = Crafty(w, h, document[canvas])
    # Cria o objeto que representa o Jogo
    return Kwarwp(game)

if __name__ == "__main__":
    main()
