"""
############################################################
Tutorial de Jogos em Python
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: 2014/10/11
:Status: This is a "work in progress"
:Revision: 0.1.0
:Home: `Labase <http://labase.selfip.org/>`__
:Copyright: 2014, `GPL <http://is.gd/3Udt>`__.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

.. _mod_crafty

"""
__version__ = "0.1.0"
from crafty import Crafty
from browser import document
from json import dumps, loads
MYID, MYPASS, START = 'private-tank-champion', 't4c6p7', 'S_T_A_R_T__'


class Tiro:
    """Representa o disparo de tiro com a torreta do canhão
    """
    def __init__(self, jogo):
        self.jogo = jogo
        self.boom = None
        # O dicionário rumo define uma direção cartesiana para cada sentido em graus
        self.rumo = {90: (256, None), 0: (None, -256),  270: (-256, None), 180: (None, 256)}
        self.tanque = None
        self.tiro = jogo.e("Tiro, 2D, Canvas,Tween, Keyboard, tiro").origin("center")
        self.tiro.bind("EnterFrame", self.colide)
        self.tiro.bind("TweenEnd", self.para)

    def anexa(self, tanque):
        """Anexa o tiro ao tanque
        """
        self.tanque = tanque
        self.tiro.attr(**tanque.posicao)
        tanque.tanque.attach(self.tiro)

    def atira(self):
        """Obtem a direção do tanque e dispara um tiro nesta direção
        """
        x, y = self.rumo[self.tanque.direcao]
        if not x is None:
            self.tiro.tween(1000, x=256+x)
        if not y is None:
            self.tiro.tween(1000, y=256+y)

    def colide(self, evento):
        """Evita a movimentação do tiro se ele colide com objetos
        """
        planta, base = self.tiro.hit("Planta"), self.tiro.hit("Base")

        def destroy(event):
            #hit[0].obj.attr(x = -1000, y=-1000)
            if base:
                Jogo.JOGO.base.destroy()  # attr(x = -1000, y=-1000)
            else:
                Jogo.JOGO.planta.destroy()  # attr(x = -1000, y=-1000)
            print(Jogo.JOGO)
            self.boom.destroy()
        if self.tiro.hit("Pedra") or base or planta:
            self.tiro.tween(1, **self.tanque.posicao)
        hit = base or planta
        if hit:
            print('colide', hit[0].obj.x)
            x, y = hit[0].obj.x, hit[0].obj.y
            self.boom = self.jogo.e("Xplode, 2D, Canvas,SpriteAnimation, explode")\
                .attr(x=x, y=y, w=64, h=64).reel('XP', 800, 0, 0, 32)\
                .animate("XP", 1)
            self.boom.bind("AnimationEnd", destroy)

    def para(self, evento):
        """Evita a movimentação do tiro se ele sai do campo
        """
        x, y = self.tanque.posicao["x"], self.tanque.posicao["y"]
        self.tiro.attr(**self.tanque.posicao)


class Tanque:
    """Representa o tanque com uma torreta de canhão
    """
    def __init__(self, jogo, tela, codigo):
        self.jogo, self.codigo = jogo, codigo
        off = int(self.codigo) % 20 * 128+128
        offx, offy = (128+off) % 320+128, (off // 512)
        print(offx, offy)
        # O dicionário rumo define uma direção em graus para cada sentido cartesiano
        self.rumo = {(0, 0): None, (1, 0): 90, (0, -1): 0, (-1, 0): 270, (0, 1): 180}
        self.tiro = Tiro(tela)
        self.tanque = tela.e("%s, 2D, Canvas, Collision, tanque" % codigo)\
            .attr(x=offx, y=offy, w=64, h=64).origin("center")
        if codigo == jogo.sid:
            self.tanque.fourway(1)
            self.tanque.bind("KeyDown", self._atira)
        self.torre = tela.e("2D, Canvas, torre")\
            .attr(x=offx, y=offy, w=64, h=64).origin("center")
        # O comando attach liga a torre ao tanque para andem juntos
        self.tanque.attach(self.torre)
        self.tiro.anexa(self)
        # O bind associa um procedimento que é executado quando acontece uma mudança de direção
        self.tanque.bind("NewDirection", self._aponta)
        self.tanque.bind("Moved", self._move)

    @property
    def posicao(self):
        """Retorna a posição do tanque.
        """
        return dict(x=self.tanque.x, y=self.tanque.y)

    @property
    def direcao(self):
        """Retorna a direção do tanque.
        """
        return self.tanque.rotation

    def _aponta(self, evento):
        self.jogo.executa(self.codigo, "aponta", x=evento.x, y=evento.y)

    def _atira(self, evento):
        #print('atira', evento.key, self.direcao, self.codigo, self.jogo)
        if evento.key == 32:
            self.jogo.send(codigo=self.codigo, operacao="atira")
            self.atira()

    def atira(self, **kwargs):
        self.tiro.atira()

    def _move(self, evento):
        self.jogo.send(codigo=self.codigo, operacao="anda", x=evento.x, y=evento.y)
        self.move(x=evento.x, y=evento.y)

    def aponta(self, x, y, **kwargs):
        #return
        direcao = self.rumo[(int(x), int(y))]
        #print("direcao", direcao)
        if not direcao is None:
            pass
            self.tanque.rotation = direcao

    def anda(self, x, y, **kwargs):
        """Evita a movimentação do tanque se ele colide com objetos
        """
        self.tanque.attr(x=x, y=y)

    def move(self, x, y, **kwargs):
        """Evita a movimentação do tanque se ele colide com objetos
        """
        def hit(tanque):
            print(tanque, tanques)
            self.tanque.attr(x=x, y=y)
        tanques = list(Jogo.TANQUES.keys())
        tanques.remove('0')
        #print(tanques)
        [self.tanque.hit(tanque) and hit(tanque) for tanque in tanques]
        if self.tanque.hit("Pedra") or self.tanque.hit("Planta")or self.tanque.hit("Base"):
            self.tanque.attr(x=x, y=y)


class Jogo:
    """Representa o jogo e posiciona as principais peças do cenário
    """
    JOGO = None
    TANQUES = {}

    def __init__(self, tela):
        Jogo.JOGO = self
        self.tela = tela
        self.sid, self.pusher = 0, None
        self.codigo = self.busca_codigo()
        tela.e("2D, Canvas, Image")\
            .attr(x=0, y=0, w=512, h=512).image("images/stage-bg.png")
        """ -XXX- Aqui teremos que adicionar a base, o objeto escuro em forma de piramide"""
        tela.sprites(90, 'images/explosion-sprites.png', explode=[0, 0])
        tela.sprites(
            64, 'images/sprites.png',
            planta=[1, 0], pedra=[1, 1], tanque=[0, 3], torre=[7, 3],
            tiro=[7, 1], base=[2, 0])
        self.planta = tela.e("Planta, 2D, Canvas, planta").attr(x=0, y=0, w=64, h=64)
        tela.e("Pedra, 2D, Canvas, pedra").attr(x=128, y=64, w=64, h=64)
        """ Aqui vai a peça que é a base animada. O valor -1 significa anima para sempre"""
        self.base = tela.e("Base, 2D, Canvas, SpriteAnimation, base").attr(x=128, y=256, w=64, h=64)\
            .attr(x=128, y=256, w=64, h=64).reel("pisca_base", 1000, 2, 0, 4)\
            .animate("pisca_base", -1)
        Jogo.TANQUES["0"] = self
        #Jogo.TANQUES[self.codigo] = Tanque(self, tela, self.codigo)
        self.inicia_conexao()

    def busca_codigo(self):
        return "1"

    def inicia_conexao(self):

        def envia(**kwargs):
            data = dict(to=MYID, **kwargs)
            print('envia', data)
            self.pusher.send(dumps(data))

        def conecta(a=None):
            print("conectou", a)
            self.pusher.send('{"setID":"' + MYID + '","passwd":"' + MYPASS + '"}')
            self.send = envia
            #self.pusher.send('{"to":"' + MYID + '","' + START + '":"' + START + '"}')

        def finaliza(codigo, operacao, **kwargs):
            print('ev.finaliza', operacao, kwargs)
            getattr(Jogo.TANQUES[codigo], operacao)(**kwargs)

        def recebe(ev):
            #return
            data = loads(ev.data)
            print('recebe ev.data', str(self.sid), ev.data)
            if 'SID' in data:
                self.sid = str(data['SID'])
                self._cria_tanque(sID=self.sid)
            elif 'operacao' in data:
                print("if ('operacao' in data) :", (str(data['sID']) != str(self.sid)))
            if ('operacao' in data) and (str(data['sID']) != str(self.sid)):
                print('CHANNEL_ in data', self.sid, data)
                finaliza(**data)
        from browser import websocket
        ws = self.pusher = websocket.websocket('ws://achex.ca:4010')

        ws.bind('open', conecta)
        ws.bind('message', recebe)

    def executa(self, codigo, operacao, **kwargs):
        self.send(codigo=codigo, operacao=operacao, **kwargs)
        getattr(Jogo.TANQUES[codigo], operacao)(**kwargs)

    def _cria_tanque(self, sID, **kwargs):
        self.send(codigo="0", operacao="cria_tanque", **kwargs)
        self.cria_tanque(sID, **kwargs)

    def recria_tanque(self, sID, **kwargs):
        print("cria_tanque", Jogo.TANQUES, sID, kwargs)
        Jogo.TANQUES[sID] = Tanque(self, self.tela, sID)

    def cria_tanque(self, sID, **kwargs):
        print("cria_tanque", Jogo.TANQUES, sID, kwargs)
        #self.send(operation="cria_tanque", codigo=codigo)
        self.pusher.send(dumps(dict(toS=sID, codigo="0", operacao="recria_tanque")))
        Jogo.TANQUES[sID] = Tanque(self, self.tela, sID)


def main():
    from crafty import Crafty
    from browser import document
    # Cria uma janela de 512x512 para o jogo na divisão do documento pydiv
    jogo = Crafty(512, 512, document["pydiv"])
    # Cria o objeto que representa o Jogo
    Jogo(jogo)

if __name__ == "__main__":
    main()
